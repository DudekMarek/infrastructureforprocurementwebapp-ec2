terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  profile = var.aws_profile
  region  = var.aws_default_region
}

#Tworzenie repozytorium dla obrazów
resource "aws_ecr_repository" "procurement_app" {
  name = "procurement_web_app"
}

resource "aws_ecr_repository" "agent" {
  name = "metrics_agent"
}

#Tworzenie security group
resource "aws_security_group" "procurment_app_sg" {
  name        = "procurment_app_sg"
  description = "Security group for procurment web app"
  vpc_id      = data.aws_vpc.default_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9090
    to_port     = 9090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Tworzenie polityki dla instancji EC2
resource "aws_iam_role" "ecs_agent" {
  name               = "ecs-agent"
  assume_role_policy = data.aws_iam_policy_document.ecs_agent.json
}

resource "aws_iam_role_policy_attachment" "ecs_agent" {
  role       = aws_iam_role.ecs_agent.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_agent" {
  name = "ecs-agent"
  role = aws_iam_role.ecs_agent.name
}

#Konfiguracja uruchomienia instancji EC2
resource "aws_launch_configuration" "ecs_launch_config" {
  name                        = "procurment-web-app-launch-configuration-5"
  image_id                    = data.aws_ami.aws_optimized_ecs.id
  instance_type               = var.instance_type
  associate_public_ip_address = true
  user_data = <<EOF
  #!/bin/bash
  echo ECS_CLUSTER=${var.cluster_name} >> /etc/ecs/ecs.config
  EOF
  security_groups      = [aws_security_group.procurment_app_sg.id]
  iam_instance_profile = aws_iam_instance_profile.ecs_agent.arn
}

#Konfiguracja AutoScaling group
resource "aws_autoscaling_group" "ecs_ec2_autoscaling_group" {
  name                      = "procurment-web-app-autoscaling-group"
  termination_policies      = ["OldestInstance"]
  default_cooldown          = 30
  health_check_grace_period = 30
  max_size                  = var.max_size
  min_size                  = var.min_size
  desired_capacity          = var.desired_capacity
  launch_configuration      = aws_launch_configuration.ecs_launch_config.name
  lifecycle {
    create_before_destroy = true
  }
  vpc_zone_identifier = [data.aws_subnet.default_subnet1.id, data.aws_subnet.default_subnet2.id]
  
  tag {
    key = "Name"
    value = var.cluster_name
    propagate_at_launch = true
  }

  depends_on = [ aws_ecs_cluster.procurement_app_cluster ]
}


#Tworzenie clustera ECS
resource "aws_ecs_cluster" "procurement_app_cluster" {
  name = var.cluster_name
}

#Tworzenie definicji zadania 
resource "aws_ecs_task_definition" "procurment_app_task_definition" {
  family                   = "procurment-app-task-definition"
  execution_role_arn       = data.aws_iam_role.taskExecutionRole.arn
  requires_compatibilities = ["EC2"]
  memory                   = 512
  cpu                      = 1024
  network_mode             = "bridge"

  container_definitions = jsonencode([
    {
      name      = "procurment-app-container"
      essential = true
      image     = var.procurment_app_image_url
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
          protocol      = "tcp"
        },
        {
          containerPort = 9090
          hostPort      = 9090
          protocol      = "tcp"
        },
      ]
      logConfiguration = {
        logDriver = "awsfirelens",
        options = {
          Name       = "grafana-loki",
          Url        = var.loki_url,
          Labels     = "{job=\"firelens\"}",
          RemoveKeys = "container_id,ecs_task_arn",
          LabelKeys  = "container_name,ecs_task_definition,source,ecs_cluster",
          LineFormat = "key_value",
        }
      }
    },
    {
      name      = "log_router"
      image     = "grafana/fluent-bit-plugin-loki"
      essential = true
      firelensConfiguration = {
        type = "fluentbit",
        options = {
          enable-ecs-log-metadata = "true",
        }
      }
      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = var.log_group,
          awslogs-region        = var.aws_default_region,
          awslogs-stream-prefix = "firelens",
        }
      }
    },
    {
      name      = "telegraf-agent-container"
      essential = true
      image     = var.telegraf_agent_image_url
      environment = [
        {
          name  = "INTERVAL"
          value = var.telegraf_interval
        },
        {
          name  = "METRICS_ENDPOINT"
          value = var.telegraf_metrisc_endpiont
        },
        {
          name  = "PROMETHEUS_ENDPOINT"
          value = var.telegraf_prometheus_endpiont
        },
        {
          name  = "PROMETHEUS_USERNAME"
          value = var.telegraf_prometheus_username
        },
        {
          name  = "PROMETHEUS_PASSWORD"
          value = var.telegraf_prometheus_password
        },
      ]
    },
  ])
}



# Uruchomienie zadania
resource "aws_ecs_service" "procurement_app_service" {
  name            = "procurment-app-service"
  cluster         = aws_ecs_cluster.procurement_app_cluster.id
  task_definition = aws_ecs_task_definition.procurment_app_task_definition.arn
  desired_count   = 1
  depends_on = [ aws_ecs_cluster.procurement_app_cluster ]
  lifecycle {
    create_before_destroy = true
  }
}
