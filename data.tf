data "aws_vpc" "default_vpc" {
  id = var.existing_vpc_id
}

data "aws_subnet" "default_subnet1" {
  id = var.existing_subnet1_id
}

data "aws_subnet" "default_subnet2" {
  id = var.existing_subnet2_id
}

data "aws_iam_role" "taskExecutionRole" {
  name = "ecsTaskExecutionRole"
}

data "aws_iam_policy_document" "ecs_agent" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_ami" "aws_optimized_ecs" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn-ami*amazon-ecs-optimized"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["591542846629"]
}

