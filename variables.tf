variable "aws_profile" {
  type    = string
  default = "default"
}

variable "aws_default_region" {
  type    = string
  default = "eu-north-1"
}

variable "existing_vpc_id" {
  type        = string
  description = "ID of default VPC for ECS"
  default     = "vpc-04090798616512c79"
}

variable "existing_subnet1_id" {
  type        = string
  description = "ID of default subnet1 for ECS"
  default     = "subnet-01e02886930fde497"
}

variable "existing_subnet2_id" {
  type        = string
  description = "ID of default subnet2 for ECS"
  default     = "subnet-06e4867da5b4625cf"
}

variable "ec2_image_id" {
  type        = string
  description = "Default AMI"
  default     = "ami-0a79730daaf45078a"
}

variable "instance_type" {
  type        = string
  description = "Type of lunched EC2 instance"
  default     = "t3.micro"
}

variable "min_size" {
  description = "Minimum EC2 instances running"
  default     = "1"
}

variable "max_size" {
  description = "Maximum EC2 instances running"
  default     = "2"
}

variable "desired_capacity" {
  description = "Desired EC2 instances running"
  default     = "1"
}

variable "cluster_name" {
  description = "Name of Cluster"
  default     = "prourment-app-cluster"
}

variable "procurment_app_image_url" {
  type        = string
  description = "Url of image of procurment web app"
  default     = "296083653718.dkr.ecr.eu-north-1.amazonaws.com/procurement_web_app:f83eedb0"
}

variable "telegraf_agent_image_url" {
  type        = string
  description = "Url of image of telegraf agent"
  default     = "296083653718.dkr.ecr.eu-north-1.amazonaws.com/metrics_agent:latest"
}

variable "telegraf_interval" {
  type        = string
  description = "INTERVAL for telegraf agent"
  default     = "10s"
}

variable "telegraf_metrisc_endpiont" {
  type        = string
  description = "METRICS_ENDPOINT for telegraf agent"
  default     = "http://13.51.193.187:9090/metrics"
}

variable "telegraf_prometheus_endpiont" {
  type        = string
  description = "PROMETHEUS_ENDPOINT for telegraf agent"
  default     = "https://prometheus-prod-24-prod-eu-west-2.grafana.net/api/prom/push"
}

variable "telegraf_prometheus_username" {
  type        = string
  description = "PROMETHEUS_USERNAME for telegraf agent"
  default     = "972701"
}

variable "telegraf_prometheus_password" {
  type        = string
  description = "PROMETHEUS_PASSWORD for telegraf agent"
  default     = "eyJrIjoiNTFkZTViZTdiOTMyOThhODY2YjI5NjQ3MjYxMDAxN2JmMzBiNTE1ZiIsIm4iOiJtZXRyaWNzLWFwaSIsImlkIjo4NDkzNTh9"
}

variable "log_group" {
  type        = string
  description = "Log group"
  default     = "loki-test"
}

variable "loki_url" {
  type        = string
  description = "Url for pushing logs to loki"
  default     = "https://588967:eyJrIjoiOTljOWEzMDMzYjhlNTVhNmIzOWEwYzQ1YzMzZTY5NTA3NGM2OTEzNyIsIm4iOiJsb2dzIiwiaWQiOjg0OTM1OH0=@logs-prod-012.grafana.net/loki/api/v1/push"
}